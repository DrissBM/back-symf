# Getting Started Symfony

This project is the first version of a larger project. For the moment it allows a user to connect to the application via gitlab, to retrieve a project (only react for the moment) and to deploy it on an online server in 1 click.

---

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

This project run on Symfony 4.4

### Prerequisites

To run this project, you need node, composer > 2.0, php > 8.0 installed on your computer

---

## Installing

Before starting to develop you need to add a file `.env.local` at the root of your project.

And fill it like this :

```
DATABASE_URL="mysql://root:root@127.0.0.1:3306/back-symf"

OAUTH_GITLAB_CLIENT_ID=d37b893939c85e23233f17b74bb33e8507326b73ddcfa7b40b668bd3ac10411e
OAUTH_GITLAB_CLIENT_SECRET=be2e25b8e66c52c5562ab44cbb53b2791bcdff13cf00bafb0c0dd2c0be0739e6

```

Now adapt the DATABASE_URL to correspond to the DB your are using.

The key used for OAUTH_GITLAB_CLIENT_ID and OAUTH_GITLAB_CLIENT_SECRET shouldn't be shared like this but for this project I'll make an exception for the comfort of the user. But those key should never be shared.

Be sure that your db is running.

Then run the command below to install all the dependecies

```
composer install
```

Now create your db

```
php bin/console doctrine:database:create
```

Then fill it

```
php bin/console doctrine:migrations:migrate
```

## Developing

While developing you will run:

```
symfony serve
```

Now you can check if your back is correctly running by following this link "http://localhost:8000"

Well done ! Now your back is running ! All you need now is to enjoy it thanks to the front.

If while running the back you encouter this error : "cURL error 60: SSL certificate problem: unable to get local issuer certificate (see https://curl.haxx.se/libcurl/c/libcurl-errors.html) for https://gitlab.com/oauth/token", follow this tuto :

## Install the front

Clone this repo https://gitlab.com/DrissBM/front-symf and follow the `README.md` of the project.

## Call API

To ease your experience, you can import the file Insomnia located at the root of the project in your insomnia to have a view of all the call you have access to.

Warning ! To make the call you need a bearer token. To get one the easiest way is to connect with the front and retrieve the token from the local storage of your browser (saved as apiToken).

## Authors

- **Driss Ben Mimoun**
