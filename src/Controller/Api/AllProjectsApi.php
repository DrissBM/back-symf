<?php

namespace App\Controller\Api;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\GitlabService;

// It return all the project the user have access to (shared with him)
class AllProjectsApi
{
    /**
     * @Route(
     *     name="getAllProjects",
     *     path="/api/gitlab_projects",
     *     methods={"GET"}
     * )
     */
    public function __invoke(GitlabService $gitlabService)
    {
        return new JsonResponse(json_decode($gitlabService->getAllProjects()));
    }
}
