<?php

namespace App\Controller\Api;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Process;
use App\Entity\Project;
use App\Service\GitlabService;
use Doctrine\ORM\EntityManagerInterface;

// This class is made to deploy on a serv by sending the project id
// The project containing all the settings will be use to throw parameter to the script
// The script will do all the work (/public/deploy.sh)
class DeployApi
{
    private $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route(
     *     name="deployProject",
     *     path="/api/deploy/{id}",
     *     methods={"GET"}
     * )
     */
    public function __invoke(GitlabService $gitlabService, $id)
    {
        // get the project in the repo
        $project = $this->em
            ->getRepository(Project::class)
            ->findOneBy(['id' => $id]);
        $project->setState("onHold");
        $this->em->persist($project);
        $this->em->flush();

        // define the script to use
        $pathToScript = "./deploy.sh";

        $accessToken = $gitlabService->getAccessToken();
        $pathWithNameSpace = $project->getGitlabPath();

        // Define the repo that will be cloned
        $repo = "https://oauth2:{$accessToken}@gitlab.com/{$pathWithNameSpace}.git";
        // Define the repo name
        $repoName = $project->getGitlabName();
        $installCmd = $project->getInstallCmd();
        $buildCmd = $project->getBuildCmd();
        $outputDir = $project->getOutputDir();
        $projectName = $project->getName();
        $branch = $project->getBranch();

        // It will run the script in $pathToScript
        $process = new Process(['sh', $pathToScript, $repo, $repoName, $branch, $installCmd, $buildCmd, $outputDir, $projectName]);
        // Define the maximum Time before stoping the process
        $process->setTimeout(3600);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            $return = new ProcessFailedException($process);
            $project->setState("failed");
        } else {
            $return = $process->getOutput();
            $project->setURL("https://sj4ckamphz.preview.infomaniak.website/{$projectName}/");
            $project->setState("success");
        }
        $this->em->persist($project);
        $this->em->flush();
        return new Response($return);
    }
}
