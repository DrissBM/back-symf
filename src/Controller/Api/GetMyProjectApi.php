<?php

namespace App\Controller\Api;

use App\Entity\User;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

// get All the project link to our user
class GetMyProjectApi
{
    private $security;
    private User $user;
    private $serializer;

    public function __construct(Security $security, EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $this->security = $security;
        $this->serializer = $serializer;
        $this->user = $this->security->getUser();
    }
    /**
     * @Route(
     *     name="getMyProject",
     *     path="/api/myproject",
     *     methods={"GET"}
     * )
     */
    public function __invoke()
    {

        $projects = $this->user->getProjects();
        $json = $this->serializer->serialize(
            $projects,
            'json'
        );
        return new Response($json);
    }
}
