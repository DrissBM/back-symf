<?php

namespace App\Controller\Api;

use App\Entity\User;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

// return the current user name
class GetUser
{
    private $security;
    private User $user;

    public function __construct(Security $security)
    {
        $this->security = $security;
        $this->user = $this->security->getUser();
    }
    /**
     * @Route(
     *     name="getUser",
     *     path="/api/myuser",
     *     methods={"GET"}
     * )
     * @param Request $request
     */
    public function __invoke()
    {
        return new Response($this->user->getName());
    }
}
