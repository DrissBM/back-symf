<?php

namespace App\Controller\Api;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\GitlabService;

// get details about a project from gitlab
class ProjectApi
{
    /**
     * @Route(
     *     name="getProject",
     *     path="/api/gitlab_projects/{id}",
     *     methods={"GET"}
     * )
     */
    public function __invoke(GitlabService $gitlabService, $id)
    {
        return new JsonResponse(json_decode($gitlabService->getProject($id)));
    }
}
