<?php

namespace App\Controller\Api;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use App\Entity\Project;
use Doctrine\ORM\EntityManagerInterface;

// This class is made to remove a project on a serv by sending the project id
// The project containing all the settings will be use to throw parameter to the script
// The script will do all the work (/public/remove.sh)
class RemoveProjectRepoApi
{
    private $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route(
     *     name="removeProjectRepo",
     *     path="/api/removeRepo/{id}",
     *     methods={"GET"}
     * )
     */
    public function __invoke($id)
    {
        // get the project in the repo
        $project = $this->em
            ->getRepository(Project::class)
            ->findOneBy(['id' => $id]);

        $this->em->remove($project);
        $return = $this->em->flush();

        return new Response("Success");
    }
}
