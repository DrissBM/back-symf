<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Project;

// Datapersister will be call for each POST, PUT, PATCH or DELETE call api (api-platform)
final class ProjectPostDataPersister implements ContextAwareDataPersisterInterface
{
    private $entityManager;
    private $security;
    private $user;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->user = $this->security->getUser();
    }

    // With this function il will define for wich instance of object the persist function will be effective
    public function supports($data, array $context = []): bool
    {
        return $data instanceof Project;
    }

    // It will add the user making the call as one of the "users" of the project automatically
    public function persist($data, array $context = [])
    {
        $data->addUser($this->user);
        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }

    public function remove($data, array $context = [])
    {
        // call your persistence layer to delete $data
    }
}
