<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $techno;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rootDir;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $buildCmd;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $outputDir;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $installCmd;

    /**
     * @ORM\Column(type="array")
     */
    private $envVar = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $URL;

    /**
     * @ORM\Column(type="integer")
     */
    private $gitlabId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gitlabName;

    /**
     * @ORM\Column(type="boolean")
     */
    private $locked;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $branch;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $commit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gitlabLastModif;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gitlabPath;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="projects")
     */
    private $users;

    public function __construct()
    {
        $this->state = "onHold";
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTechno(): ?string
    {
        return $this->techno;
    }

    public function setTechno(string $techno): self
    {
        $this->techno = $techno;

        return $this;
    }

    public function getRootDir(): ?string
    {
        return $this->rootDir;
    }

    public function setRootDir(string $rootDir): self
    {
        $this->rootDir = $rootDir;

        return $this;
    }

    public function getBuildCmd(): ?string
    {
        return $this->buildCmd;
    }

    public function setBuildCmd(string $buildCmd): self
    {
        $this->buildCmd = $buildCmd;

        return $this;
    }

    public function getOutputDir(): ?string
    {
        return $this->outputDir;
    }

    public function setOutputDir(string $outputDir): self
    {
        $this->outputDir = $outputDir;

        return $this;
    }

    public function getInstallCmd(): ?string
    {
        return $this->installCmd;
    }

    public function setInstallCmd(string $installCmd): self
    {
        $this->installCmd = $installCmd;

        return $this;
    }

    public function getEnvVar(): ?array
    {
        return $this->envVar;
    }

    public function setEnvVar(array $envVar): self
    {
        $this->envVar = $envVar;

        return $this;
    }

    public function getURL(): ?string
    {
        return $this->URL;
    }

    public function setURL(?string $URL): self
    {
        $this->URL = $URL;

        return $this;
    }

    public function getGitlabId(): ?int
    {
        return $this->gitlabId;
    }

    public function setGitlabId(int $gitlabId): self
    {
        $this->gitlabId = $gitlabId;

        return $this;
    }

    public function getGitlabName(): ?string
    {
        return $this->gitlabName;
    }

    public function setGitlabName(string $gitlabName): self
    {
        $this->gitlabName = $gitlabName;

        return $this;
    }

    public function getLocked(): ?bool
    {
        return $this->locked;
    }

    public function setLocked(bool $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    public function getBranch(): ?string
    {
        return $this->branch;
    }

    public function setBranch(string $branch): self
    {
        $this->branch = $branch;

        return $this;
    }

    public function getCommit(): ?string
    {
        return $this->commit;
    }

    public function setCommit(string $commit): self
    {
        $this->commit = $commit;

        return $this;
    }

    public function getGitlabLastModif(): ?string
    {
        return $this->gitlabLastModif;
    }

    public function setGitlabLastModif(string $gitlabLastModif): self
    {
        $this->gitlabLastModif = $gitlabLastModif;

        return $this;
    }

    public function getGitlabPath(): ?string
    {
        return $this->gitlabPath;
    }

    public function setGitlabPath(string $gitlabPath): self
    {
        $this->gitlabPath = $gitlabPath;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }
}
