<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class GitlabService
{
    private $client;
    private $params;

    public function __construct(HttpClientInterface $client, Security $security, ContainerBagInterface $params)
    {
        $this->client = $client;
        $this->security = $security;
        $this->params = $params;
    }

    public function getAllProjects(): string
    {
        // Authorization: Bearer OAUTH-TOKEN
        $userGitlabToken = $this->security->getUser()->getAccessToken();

        $urlGitlabApi = $this->params->get('gitlab.api_url');
        $url = $urlGitlabApi . "projects?membership=true";
        $response = $this->client->request(
            'GET',
            $url,
            [
                'auth_bearer' => $userGitlabToken
            ]
        );

        return $response->getContent();
    }

    public function getProject(string $id): string
    {
        $userGitlabToken = $this->getAccessToken();
        $urlGitlabApi = $this->params->get('gitlab.api_url');
        $url = $urlGitlabApi . "projects/" . $id;
        $response = $this->client->request(
            'GET',
            $url,
            [
                'auth_bearer' => $userGitlabToken
            ]
        );
        $url2 = "https://gitlab.com/api/v4/" . "projects/" . $id . "/repository/branches";
        $response2 = $this->client->request(
            'GET',
            $url2,
            [
                'auth_bearer' => $userGitlabToken
            ]
        );

        $content = $response->getContent();
        $content2 = $response2->getContent();
        // $content = '{"id":521583, "name":"symfony-docs", ...}'

        return "[" . $content . "," . $content2 . "]";
    }

    public function getAccessToken(): string
    {
        return $this->security->getUser()->getAccessToken();
    }
}
